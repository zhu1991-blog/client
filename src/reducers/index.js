import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import authReducer from './auth';
import projectReducer from './project';
import userReducer from './user';

export default combineReducers({
    auth: authReducer,
    form: formReducer,
    project: projectReducer,
    user: userReducer
});



