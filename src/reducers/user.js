import * as types from '../actions/types';

const initState = {
    user: null,
    loading: true,
    updating: false,
    error: false
}

const reducer = (state = initState, action) => {
    switch (action.type) {
        case types.pending(types.FETCH_USER_SIMPLE):
        case types.pending(types.FETCH_USER):
            return {
                ...state,
                loading: true
            };
        case types.fulfilled(types.FETCH_USER_SIMPLE):
        case types.fulfilled(types.FETCH_USER):
            return {
                ...state,
                loading: false,
                user: {
                    ...action.payload,
                    skills: action.payload.skills.map(skill => (skill.name))
                }
            }
        case types.rejected(types.FETCH_USER_SIMPLE):
        case types.rejected(types.FETCH_USER):
            return {
                ...state,
                error: true,
                loading: false
            }
        case types.pending(types.UPDATE_USER):
            return {
                ...state,
                updating: true,
                error: false
            }
        case types.fulfilled(types.UPDATE_USER):
            return {
                ...state,
                user:{ 
                    ...action.payload,
                    skills: action.payload.skills.map(skill => (skill.name))
                },
                updating: false
            }
        case types.rejected(types.UPDATE_USER):
            return {
                ...state,
                updating: false,
                error: true
            }
        default:
            return state;
    }
}

export default reducer;