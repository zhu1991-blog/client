import * as types from '../actions/types';

const initState = {
    rootPath: null,
    token: null,
    loading: false,
    error: null,
    cc: null,
    ccError: false,
    count: null
};

export default (state = initState, action) => {
    switch(action.type) {
        case types.pending(types.CC_SUBMIT):
        case types.pending(types.CC_CHECK):
            return {
                ...state,
                ccError: false
            }
        case types.pending(types.AUTH_LOGIN):
        case types.pending(types.AUTH_CHECK):
            return {
                ...state,
                token: null,
                loading: true,
                cc: null,
                count: null
            }
        case types.fulfilled(types.AUTH_LOGOUT):
            return {
                ...state,
                rootPath: null,
                token: null,
                loading: false,
                error: null,
                cc: null,
                count: null
            }
        case types.fulfilled(types.AUTH_CHECK):
        case types.fulfilled(types.AUTH_LOGIN):
            return {
                ...state,
                rootPath: action.payload.username,
                token: action.payload.token,
                loading: false,
                error: null,
                cc: action.payload.cc,
                count: action.payload.count
            }
        case types.rejected(types.AUTH_CHECK):
        return {
            ...state,
            token: null,
            loading: false,
        }
        case types.rejected(types.AUTH_LOGIN):
            return {
                ...state,
                token: null,
                loading: false,
                error: "Login Failed"
            }
        case types.fulfilled(types.CC_CHECK):
        case types.fulfilled(types.CC_SUBMIT):
            return {
                ...state,
                cc: action.payload.cc
            }
        case types.rejected(types.CC_SUBMIT):
            return {
                ...state,
                ccError: true
            }
        case types.rejected(types.CC_CHECK):
            return {
                ...state
            }
        default:
            return state;
    }
}