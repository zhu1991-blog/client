import * as types from '../actions/types';
import * as dateUtil from '../util/date';


const initState = {
    projects: [],
    loading: false,
    submitting: false,
    deleting: false,
    error: false,
    project: {},
    pracProjects: null,
    projProjects: null
}

const reducer = (state = initState, action) => {
    switch (action.type) {
        case types.pending(types.UPDATE_PROJECT):
        case types.pending(types.ADD_PROJECT):
            return {
                ...state,
                submitting: true,
                error: false
            }
        case types.pending(types.FETCH_PRAC_PROJECTS):
            return {
                ...state,
                pracProjects: null
            }
        case types.pending(types.FETCH_PROJ_PROJECTS):
            return {
                ...state,
                projProjects: null
            }
        case types.pending(types.FETCH_PROJECTS):
            return {
                ...state,
                loading: true
            }
        case types.pending(types.DELETE_PROJECT):
            return {
                ...state,
                deleting: true,
                error: true
            }
        case types.fulfilled(types.ADD_PROJECT):
            return {
                ...state,
                projects: [...action.payload].concat(state.projects),
                submitting: false
            }
        case types.rejected(types.ADD_PROJECT):
            return {
                ...state,
                submitting: false,
                error: true
            }
        case types.fulfilled(types.FETCH_PROJECTS):
            return {
                ...state,
                projects: action.payload,
                loading: true
            }
        case types.rejected(types.DELETE_PROJECT):
            return {
                ...state,
                deleting: false,
                error: true
            }
        case types.fulfilled(types.DELETE_PROJECT):
            return {
                ...state,
                deleting: false,
                projects: state.projects.filter(p => p._id !== action.payload._id)
            }
        case types.SELECT_PROJECT:
            return {
                ...state,
                project: {
                    ...action.payload.project,
                    startTime: dateUtil.toString(new Date(action.payload.project.startTime), '-'),
                    endTime: dateUtil.toString(new Date(action.payload.project.endTime), '-')
                }
            };
        case types.fulfilled(types.UPDATE_PROJECT):
            const proejctIdx = state.projects.findIndex(project => project._id === action.payload._id);
            return {
                ...state,
                submitting: false,
                projects: [
                    ...state.projects.slice(0, proejctIdx),
                    action.payload,
                    ...state.projects.slice(proejctIdx + 1)
                ]
            }
        case types.rejected(types.UPDATE_PROJECT):
            return {
                ...state,
                submitting: false,
                error: true
            }
        case types.fulfilled(types.FETCH_PRAC_PROJECTS):
            return {
                ...state,
                loading: false,
                pracProjects: action.payload
            }
        case types.fulfilled(types.FETCH_PROJ_PROJECTS):
            return {
                ...state,
                loading: false,
                projProjects: action.payload
            }
        case types.rejected(types.FETCH_PRAC_PROJECTS):
            return {
                ...state,
                loading: false,
                pracProjects: null
            }
        case types.rejected(types.FETCH_PROJ_PROJECTS):
            return {
                ...state,
                loading: false,
                projProjects: null
            }
        default:
            return state;
    }
}

export default reducer;