import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import promise from 'redux-promise-middleware';

import reducers from './reducers';

const middlewares = [
    thunk,  promise()
];

if (process.env.NODE_ENV !== 'production') {
    middlewares.push(logger);
}

export default createStore(
    reducers, 
    applyMiddleware(...middlewares)
);