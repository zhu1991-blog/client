import axios from 'axios';
import { createAction } from 'redux-actions';
import cookies from 'react-cookies';

import * as types from './types';

export const login = createAction(
    types.AUTH_LOGIN,
    async (username, password) => {
        const res = await axios.post('/auth/login', {username, password});
        await localStorage.setItem('token', res.data.token);
        return res.data
    }
);

export const logout = createAction(
    types.AUTH_LOGOUT,
    async () => {
        const res = await axios.get('/auth/logout');
        await localStorage.removeItem('token');
        await localStorage.removeItem('cc');
        return res.data;
    }
);

export const checkAuth = createAction(
    types.AUTH_CHECK,
    async () => {
        const res = await axios.post('/auth/check', {token: localStorage.getItem('token')});
        return res.data;
    }
) 

export const checkCc = createAction(
    types.CC_CHECK,
    async (username) => {
        const res = await axios.post('/auth/cc', {cc: localStorage.getItem('cc'), username});
        return res.data;
    }
)

export const submitCc = createAction(
    types.CC_SUBMIT,
    async (cc, username) => {
        const res = await axios.post('/auth/cc', {cc: cc || localStorage.getItem('cc'), username});
        localStorage.setItem('cc', res.data.cc);
        return res.data;
    }
)