import axios from 'axios';
import { createAction } from 'redux-actions';

import * as types from './types';

export const fetchUser = createAction(
    types.FETCH_USER,
    async (username) => {
        const res = await axios.get(`/api/user/${username}`, {
            headers: {
                auth: localStorage.getItem('token'),
                cc: localStorage.getItem('cc')
            }
        });
        return res.data;
    }
);

export const fetchUserSimple = createAction(
    types.FETCH_USER,
    async (username) => {
        const res = await axios.get(`/api/user/${username}/simple`);
        return res.data;
    }
);

export const updateUser = createAction(
    types.UPDATE_USER,
    async (username, user) => {
        const res = await axios.put('/api/user/' + username, user, {
            headers: {
                auth: localStorage.getItem('token'),
            }
        });
        return res.data;
    }
);