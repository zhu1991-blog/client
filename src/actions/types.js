const FULFILLED = '_FULFILLED';
const REJECTED = '_REJECTED';
const PENDING = '_PENDING';

export const fulfilled = (type) => type + FULFILLED;
export const rejected = (type) => type + REJECTED;
export const pending = (type) => type + PENDING;

export const FETCH_GITHUB_REPOSITORIES = 'FETCH_GITHUB_REPOSITORIES';

export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_CHECK = 'AUTH_CHECK';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const CC_CHECK = 'CC_CHECK';
export const CC_SUBMIT = 'CC_SUBMIT';

export const ADD_PROJECT = 'ADD_PROJECT';
export const FETCH_PROJECTS = 'FETCH_PROJECTS';
export const FETCH_PRAC_PROJECTS = 'FETCH_PRAC_PROJECTS';
export const FETCH_PROJ_PROJECTS = 'FETCH_PROJ_PROJECTS';
export const FETCH_PROJECT = 'FETCH_PROJECT';
export const DELETE_PROJECT = 'DELETE_PROJECT';
export const UPDATE_PROJECT = 'UPDATE_PROJECT';
export const SELECT_PROJECT = 'SELECT_PROJECT';

export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SIMPLE = 'FETCH_USER_SIMPLE';
export const UPDATE_USER = 'UPDATE_USER';
