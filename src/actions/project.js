import { createAction } from 'redux-actions';
import axios from 'axios';

import * as types from './types';

export const addProject = createAction(types.ADD_PROJECT, async (project, username) => {
    let res = await axios.get('/api/user/' + username + '/simple');
    res = await axios.post('/api/project/' + res.data._id, project, {
        headers: {
            auth: localStorage.getItem('token')
        }
    });
    return res.data;
});

export const fetchProjects = createAction(types.FETCH_PROJECTS, async (username) => {
    let res = await axios.get('/api/user/' + username + '/simple');
    res = await axios.get('/api/project/' + res.data._id, {
        headers: {
            auth: localStorage.getItem('token')
        }
    });
    return res.data;
});

export const deleteProject = createAction(types.DELETE_PROJECT, async (projectId, username) => {
    let res = await axios.get('/api/user/' + username + '/simple');
    res = await axios.delete(`/api/project/${res.data._id}/${projectId}`, {
        headers: {
            auth: localStorage.getItem('token')
        }
    });
    return res.data;
});

export const fetchProject = createAction(types.FETCH_PROJECT, async (projectId, username) => {
    let res = await axios.get('/api/user/' + username + '/simple');
    res = await axios.get(`/api/project/${res.data._id}/${projectId}`, {
        headers: {
            auth: localStorage.getItem('token')
        }
    });
    return res.data;
});

export const selectProject = createAction(types.SELECT_PROJECT, (project) => ({
    project
}));

export const updateProject = createAction(types.UPDATE_PROJECT, async (projectId, project, username) => {
    let res = await axios.get('/api/user/' + username + '/simple');
    res = await axios.put(`/api/project/${res.data._id}/${projectId}`, project, {
        headers: {
            auth: localStorage.getItem('token')
        }
    });
    return res.data;
});

export const fetchPracProjects = createAction(types.FETCH_PRAC_PROJECTS, async (username) => {
    let res = await axios.get('/api/user/' + username + '/simple');
    res = await axios.get(`/api/project/${res.data._id}/prac`);
    return res.data;
});

export const fetchProjProjects = createAction(types.FETCH_PROJ_PROJECTS, async (username) => {
    let res = await axios.get('/api/user/' + username + '/simple');
    console.log(localStorage.getItem('token'));
    res = await axios.get(`/api/project/${res.data._id}/proj`, {
        headers: {
            auth: localStorage.getItem('token'),
            cc: localStorage.getItem('cc')
        }
    });
    return res.data;
});