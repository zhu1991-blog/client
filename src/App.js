import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import asyncComponent from './hoc/asyncComponent';
import Aux from './hoc/Aux';
import { checkAuth } from './actions';

const Header = asyncComponent(() => import('./components/Header'));
const Home = asyncComponent(() => import('./components/Home'));
const Profile = asyncComponent(() => import('./components/Profile/Profile'));
const ProfileForm = asyncComponent(() => import('./components/Profile/ProfileForm'));

class App extends React.Component {

    componentDidMount() {
        this.props.checkAuth();
    }

    render() {
        return (
            <BrowserRouter>
                <Aux>
                    <header>
                        <Header />
                    </header>
                    <main>
                        <div className="row">
                            <div className="col s12 offset-m1 m10">
                                <div className="container" style={{ marginTop: '20px' }}>
                                    <Switch>
                                        <Route path="/editProfile" component={ProfileForm} />
                                        <Route path="/profile" component={Profile} />
                                        <Route path="/" component={Home} />
                                    </Switch>
                                </div>
                            </div>
                        </div>
                    </main>
                    <footer>
                    </footer>
                </Aux>
            </BrowserRouter>
        );
    }
};

export default connect(null, { checkAuth })(App);