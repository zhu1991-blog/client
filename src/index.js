import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'materialize-css/dist/css/materialize.min.css'

import App from './App';
import store from './store';
import axios from 'axios';

if (process.env.NODE_ENV !== 'production') {
    window.axios = axios;
} else {
    axios.defaults.baseURL = 'https://safe-harbor-86032.herokuapp.com';
}

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.querySelector('#root')
);

