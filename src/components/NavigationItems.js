import React from 'react';
import NavigationItem from './NavigationItem';

const NavigationItems = ({id, classes, onItemSelected, isAuth, onLogout, onLogin, rootPath}) => {
    const items = [
        <NavigationItem
            key="profile"
            name="Profile"
            path={`/profile`}
            icon="person"
            onItemSelected={onItemSelected}
        />
    ];
    if (isAuth) {
        items.push(
            <NavigationItem 
                key="logout"
                name="Logout"
                icon="security"
                onItemSelected={onLogout}
            />
        );
    } else {
        items.push(
            <NavigationItem 
                key="login"
                name="Login"
                icon="security"
                onItemSelected={onLogin}
            />
        )
    }

    return (
        <ul className={classes} id={id}>
           {items}
        </ul>
    );
};




export default NavigationItems;