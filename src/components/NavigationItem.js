import React from 'react';

const NavigationItem = ({name, path, onItemSelected, icon}) => (
    <li>
        <a onClick={() => onItemSelected(path)}>
            {name}
            {icon ? <i className="material-icons left">{icon}</i> : null}
        </a>
    </li>
);

export default NavigationItem;