import React from 'react';
import { reduxForm, Field } from 'redux-form';
import _ from 'lodash';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import CField from '../common/Field';
import { updateUser, fetchUser } from '../../actions';
import Aux from '../../hoc/Aux';
import Icon from '../common/Icon';

const FIELDS = [
    { name: "name", type: "text", label: "Name", classes: "col s12 m6" },
    { name: "fullName", type: "text", label: "Full Name", classes: "col s12 m6" },
    { name: "skills", type: "textarea", label: "Skills" },
    { name: "simpleProfile", type: "textarea", label: "Simple Profile" },
    { name: "profile", type: "textarea", label: "Profile" },
];

class ProfileForm extends React.Component {

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.updating && !this.props.error && !this.props.updating) {
            this.props.history.goBack();
        }
    }

    componentDidMount() {
        this.props.fetchUser('memory140662');
    }

    render() {
        let { handleSubmit, updateUser, initialValues, loading, isAuth } = this.props;


        if (loading || !initialValues) {
            return (
                <div></div>
            );
        }

        let redirect = null;
        if (!isAuth) {
            redirect = <Redirect to={`/`} />;
        }

        let fields = _.map(FIELDS, field =>
            <Field key={field.name} {...field} component={CField} />
        );
        return (
            <Aux>
                {redirect}
                <form>
                    <div className="row">
                        {fields}
                    </div>
                    <button 
                        className="red btn left" 
                        type="button" onClick={() => { this.props.history.goBack(); }}
                    >
                        <Icon classes="left">
                            cancel
                        </Icon>
                        Cancel
                    </button>
                    <button 
                        className="btn right" 
                        type="button" 
                        onClick={handleSubmit(values => {updateUser(initialValues.username, {
                            ...initialValues,
                            ...values,
                            skills: _.split(values.skills, ',').map(skill => ({ name: _.trim(skill) }))
                        });
                    })}>
                        <Icon classes="left">
                            save
                        </Icon>
                        Save
                    </button>
                </form>
            </Aux>
        );

    }
};

const config = {
    form: 'profile',
    enableReinitialize: true
}

const mapStateToProps = state => ({
    initialValues: state.user.user,
    loading: state.user.loading,
    updating: state.user.updating,
    error: state.user.error,
    isAuth: !!state.auth.token,
});

export default connect(mapStateToProps, { updateUser, fetchUser })(reduxForm(config)(ProfileForm));