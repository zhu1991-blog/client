import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from '../../actions';
import Modal from '../common/Modal';
import ProfileDetail from './ProfileDetail';
import ProfileSimpleDetail from './ProfileSimpleDetail';
import ProjectDetail from '../Project/ProjectDetail';
import CcForm from './CcForm';
import Icon from '../common/Icon';

class Profile extends React.Component {

    state = {
        isSimple: true,
        showCcModal: false,
        showProjectModal: false,
        project: null,
        isInit: true,
    }

    switchMode = () => {
        if (this.state.isSimple) {
            if (this.props.isAuth || this.props.cc) {
                this.setState({ isSimple: false });
            } else {
                this.setState({ showCcModal: true });
            }
        } else {
            this.setState({ isSimple: true });
        }
    }


    componentDidUpdate(prevProps, prevState) {
        if (this.state.project && prevState.project !== this.state.project) {
            this.setState({ showProjectModal: true });
        }

        if (prevProps.cc !== this.props.cc && this.props.cc && !this.state.isInit) {
            this.switchMode();
            this.setState({ showCcModal: false });
        }

        if (!this.props.cc && !this.props.isAuth && !prevState.isSimple) {
            this.setState({ isSimple: true });
        }
    }

    componentDidMount() {
        this.props.checkCc('memory140662');
        this.setState({ isInit: true });
    }


    submitHandler = (cc) => {
        this.props.submitCc(cc, 'memory140662');
        this.setState({ isInit: false });
    }

    render() {
        let edit = null;
        let cc = null;
        if (this.props.isAuth) {
            cc = <div className="left" style={{margin: '5px 0'}}>
                驗證碼:
                <span style={{
                    margin: '5px',
                    background: '#CCC',
                    fontSize: '24px',
                    padding: '3px 10px',
                    borderRadius: '15px'
                }}>
                    {this.props.cc}
                </span>
                &nbsp;
            </div>;
            edit = (
                <Link
                    to={`/editProfile`}
                    className="btn"
                    style={{ margin: '5px 0', width: '100%'}}>
                        <Icon classes="left">edit</Icon>
                        Edit
                </Link>
            );
        }

        let profileDetail = <ProfileSimpleDetail
            user={this.props.user}
            fetchUser={() => this.props.fetchUserSimple('memory140662')}
            fetchPracProjects={() => this.props.fetchPracProjects('memory140662')}
            projects={this.props.pracProjects}
            switchProject={project => this.setState({ project })}
        />
        if (!this.state.isSimple) {
            profileDetail = <ProfileDetail
                user={this.props.user}
                fetchUser={() => this.props.fetchUser('memory140662')}
                fetchPracProjects={() => this.props.fetchPracProjects('memory140662')}
                fetchProjProjects={() => this.props.fetchProjProjects('memory140662')}
                projects={this.props.projProjects}
                pracProjects={this.props.pracProjects}
                switchProject={project => this.setState({ project })}
            />
        }
        return (
            <div>
                <div className="center-align">
                    <a 
                        onClick={this.switchMode}
                        className="btn-flat"
                        style={{
                            color: '#039be5'
                        }}
                    >
                        <Icon classes="left" >arrow_forward</Icon>
                            點擊切換至{this.state.isSimple ? '完整' : '簡易'}版履歷
                        <Icon classes="right" >arrow_back</Icon>
                    </a>
                </div>
                {profileDetail}
                <div className="row">
                    <div className="col s12 m8">
                        {cc}
                    </div>
                    <div className="col s12 m4">
                        {edit}
                    </div>
                </div>
                <Modal id="cc" show={this.state.showCcModal}
                        onClosed={() => this.setState({ showCcModal: false })}
                        onClose={() => this.setState({ showCcModal: false })}>
                        <CcForm onSubmit={this.submitHandler} />
                    </Modal>
                    <Modal id="project" show={this.state.showProjectModal}
                        onClose={() => this.setState({ showProjectModal: false, project: null })}
                        onClosed={() => this.setState({ showProjectModal: false, project: null })}>
                        <ProjectDetail project={this.state.project} />
                    </Modal>
                </div>
                );
            }
        }
        
const mapStateToProps = ({auth, user, project: {projProjects, pracProjects } }) => ({
                    isAuth: !!auth.token,
                rootPath: auth.rootPath,
                user: user.user,
                loading: user.loading,
                projProjects,
                pracProjects,
                cc: auth.cc
            });
            
export default connect(mapStateToProps, actions)(withRouter(Profile));