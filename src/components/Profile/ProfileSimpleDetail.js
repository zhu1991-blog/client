import React from 'react';
import _ from 'lodash';
import renderHTML from 'react-render-html';

import Aux from '../../hoc/Aux';
import Card from '../common/Card';
import Spinner from '../common/Spinner';
import ProjectCard from '../Project/ProjectCard';

class ProfileSimpleDetail extends React.Component {

    componentDidMount() {
        this.props.fetchUser();
        this.props.fetchPracProjects();
    }
    

    render() {
        const { user, projects, switchProject } = this.props;

        if (!user) {
            return <div></div>;
        }

        let projectCards = <Spinner />;
        if (projects) {
            projectCards = _.map(projects, project => (
                <div key={project._id} className="col s12 m6 l6">
                    <ProjectCard 
                        name={project.name} 
                        desc={project.desc} 
                        goDetail={() => switchProject(project)}
                        mainLang={project.mainLang}
                        truncate/>
                </div>
            ));
        }

        return (
            <Aux>
                <h2>{user.name}</h2>
                <p>{renderHTML(user.simpleProfile)}</p>
                <hr />
                <h5>一些小小的練習</h5>
                <div className="row">
                    {projectCards}
                </div>
            </Aux>
        );
    }
};

export default ProfileSimpleDetail;