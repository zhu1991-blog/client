import React from 'react';
import _ from 'lodash';
import M from 'materialize-css';
import renderHTML from 'react-render-html';

import Aux from '../../hoc/Aux';
import Card from '../common/Card';
import Spinner from '../common/Spinner';
import Chip from '../common/Chip';
import Section from '../common/Section';
import Field from '../common/Field';
import ProjectCard from '../Project/ProjectCard';
import ProfileImg from '../../assets/profile.png';


class ProfileDetail extends React.Component {

    componentDidMount() {
        this.props.fetchUser();
        this.props.fetchProjProjects();
        this.props.fetchPracProjects();
    }

    render() {
        const { user, pracProjects, projects, switchProject } = this.props;

        if (!user) {
            return <div></div>;
        }

        let skills = _.map(user.skills, (skill, index) => (
            <Chip key={index}>{skill}</Chip>
        ));

        let pracCards = <Spinner />;
        if (pracProjects) {
            pracCards = _.map(pracProjects, project => (
                <div key={project._id} className="col s12 m6 l6">
                    <ProjectCard 
                        name={project.name} 
                        desc={project.desc} 
                        goDetail={() => switchProject(project)}
                        mainLang={project.mainLang}
                        truncate/>
                </div>
            ));
        }
        let projCards = <Spinner />;
        if (projects) {
            projCards = _.map(projects, project => (
                <div key={project._id} className="col s12 m6 l6">
                    <ProjectCard 
                        name={project.name} 
                        desc={project.desc} 
                        goDetail={() => switchProject(project)}
                        mainLang={project.mainLang}
                        truncate/>
                </div>
            ));
        }

        return (
            <Aux>
                <h4>
                    <img 
                        src={user.profileImg ? user.profileImg : ProfileImg} 
                        className="responsive-img circle"
                        style={{width: '100px', height: '100px'}}/>
                    <span style={{ marginLeft: '5px' }}>{user.fullName}</span>
                </h4>
                <Section title="常使用的技術">
                    {skills}
                </Section>
                <Section title="關於自己">
                    <p style={{ whiteSpace: 'pre-wrap' }}>
                        {user.profile ? renderHTML(user.profile): null}
                    </p>
                </Section>
                <Section title="專案經歷">
                    <div className="row">
                        {projCards}
                    </div>
                </Section>
                <Section title="個人作品及練習">
                    <div className="row">
                        {pracCards}
                    </div>
                </Section>
            </Aux>
        );
    }
};

export default ProfileDetail;