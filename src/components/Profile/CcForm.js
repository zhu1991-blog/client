import React from 'react';
import { connect } from 'react-redux';

import Field from '../common/Field';

const CcForm = ({onSubmit, error, cc}) => {
    return (
        <form style={{margin: '10px'}} onSubmit={e => {
            e.preventDefault();
            onSubmit(e.target.cc.value);
        }}>
            <Field 
                input={{name: 'cc', defaultValue: cc || "" }} 
                label="請輸入驗證碼"
                focus/>
            <div>
                <p className="left red-text">{error ? "驗證碼錯誤": ""}</p>
                <div className="row">
                    <button className="col s12 m2 btn right">提交</button>
                </div>
            </div>
        </form>
    );
};

export default connect(state => ({error: state.auth.ccError}))(CcForm);