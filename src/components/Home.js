import React from 'react';
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router-dom';

import FloatingButton from './common/FloatingButton';
import ProjectForm from './Project/ProjectForm';
import Modal from './common/Modal';
import { addProject, fetchProjects, deleteProject, selectProject, updateProject } from '../actions'
import ProjectList from './Project/ProjectList';

class Home extends React.Component {

    state = {
        modalShow: false,
        project: {}
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.modalShow && !this.props.error && !this.props.submitting) {
            this.setState({ modalShow: false });
        }
    }

    componentDidMount() {
        this.props.fetchProjects('memory140662');
    }

    onSubmit = (values, projectId) => {
        if (projectId) {
            this.props.updateProject(projectId, values, 'memory140662');
        } else {
            this.props.addProject(values, 'memory140662');
        }
    }

    onDelete = (projectId) => {
        this.props.deleteProject(projectId, 'memory140662');
    }

    goDetail = (project) => {
        this.props.selectProject(project);
        this.setState({modalShow: true});
    }

    onModalClosed = () => {
        this.props.selectProject({});
        this.setState({modalShow: false})
    }

    render() {
        let redirect = null;
        if (!this.props.isAuth) {
            redirect = <Redirect to={`/profile`}/>
        }

        return (
            <div>
                {redirect}
                <ProjectList 
                    projects={this.props.projects}
                    onDelete={this.onDelete}
                    goDetail={this.goDetail}
                />
                <FloatingButton
                    dataTarget="projectForm"
                    icon="add"
                    classes="modal-trigger"
                    clicked={() => this.goDetail({})}
                />
                <Modal
                    id="projectForm"
                    show={this.state.modalShow}
                    onClosed={this.onModalClosed}
                    onClose={this.onModalClosed}
                    dismissible="false"
                >
                    <ProjectForm
                        onSubmit={values => this.onSubmit(values, this.props.projectId)}
                        isAdd={!this.props.projectId}
                    />
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = ({ project, auth }) => ({
    projects: project.projects,
    loading: project.loading,
    submitting: project.submitting,
    error: project.error,
    isAuth: !!auth.token,
    rootPath: auth.rootPath,
    projectId: project.project._id
});

const mapDispatchToProps = {
    addProject,
    fetchProjects,
    deleteProject,
    selectProject,
    updateProject
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));