import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';

import NavigationItems from './NavigationItems';
import LoginForm from './LoginForm';
import * as actions from '../actions/auth';
import Modal from './common/Modal';
import Sidenav from './common/Sidenav';

class Header extends Component {

    state = {
        show: false,
        path: null,
        showSidenav: false
    }

    onItemSelected = (path) => {
        this.closeSidenav()
        this.props.history.push(path);
    }

    onLogout = () => {
        this.closeSidenav()
        this.props.doLogout();
    }

    openSidenav = () => {
        this.setState({ showSidenav: true});
    }

    closeSidenav = () => {
        this.setState({ showSidenav: false});
    }

    setPath = (props) => {
        // const username = _.split(props.location.pathname, '/')[1];
        this.setState({path: ""});
    }

    componentDidMount() {
        this.setPath(this.props);
    }
    
    doLogin = () => {
        this.closeSidenav();
        this.setState({ show: true });
    }

    closeLoginForm = () => {
        this.setState({show: false});
    }

    componentDidUpdate(prevProps, prevState) {
        if (!this.props.error && prevState.show) {
            this.closeLoginForm();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.location &&  nextProps.location !== this.props.location) {
            this.setPath(nextProps);
        }
    }
    
    render() {
        const { path } = this.state;
        return (
            <div>
                <nav>
                    <div className="nav-wrapper">
                        <div className="container">
                            <Link
                                to={`/${this.props.isAuth ? "" : "profile"}`}
                                className="brand-logo"
                            >
                                <div className="valign-wrapper">
                                    <span style={{ fontFamily: "'Indie Flower', cursive" }}>
                                        ZHU++
                                    </span>
                                    {this.props.isAuth ? <span className="badge grey lighten-3">{this.props.count}</span>: null}
                                </div>
                            </Link>
                            <a
                                onClick={this.openSidenav}
                                className="sidenav-trigger"
                                data-target="mobile"
                            >
                                <i className="material-icons">menu</i>
                            </a>
                            <NavigationItems 
                                isAuth={this.props.isAuth}
                                classes="right hide-on-med-and-down"
                                onItemSelected={this.onItemSelected}
                                onLogout={this.onLogout}
                                onLogin={this.doLogin}
                                rootPath={path}
                            />
                        </div>
                    </div>
                </nav>
                <Sidenav 
                    id="mobile" 
                    show={this.state.showSidenav}
                >
                    <NavigationItems 
                        isAuth={this.props.isAuth}
                        onItemSelected={this.onItemSelected}
                        onLogout={this.onLogout}
                        onLogin={this.doLogin}
                        rootPath={path}
                    />
                </Sidenav>
                <Modal 
                    id="loginForm" 
                    show={this.state.show}
                    dismissible="false"
                    onClose={this.closeLoginForm}
                >
                    <LoginForm />
                </Modal>
            </div>
        );
    }
};

const mapStateToProps = state => ({
    isAuth: !!state.auth.token,
    error: state.auth.error,
    rootPath: state.auth.rootPath,
    count: state.auth.count
});

const mapDispatchToProps = dispatch => ({
    doLogout: () => dispatch(actions.logout())
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));