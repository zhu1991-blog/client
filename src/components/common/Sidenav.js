import React from 'react';
import M from 'materialize-css';

class Sidenav extends React.Component {
    state = { 
        children: null
    }

    onOpenStart = () => {
        this.setState({ children: this.props.children });
        this.onStatusChanged('open');
    }

    onCloseEnd = () => {
        this.setState({ children: null });
        this.onStatusChanged('close');
    }

    onStatusChanged = (status) => {
        if (this.props.onStatusChanged) {
            this.props.onStatusChanged(status);
        }
    }

    componentDidMount() {
        const options = M.Sidenav.init(document.querySelector(`#${this.props.id}`)).options;
        options.onOpenStart = this.onOpenStart;
        options.onCloseEnd = this.onCloseEnd;

        this.toggleSidenav(this.props.show);
    }

    componentWillReceiveProps(nextProps) {
        this.toggleSidenav(nextProps.show);
    }

    toggleSidenav = (isShow) => {
        if (isShow) {
            M.Sidenav.getInstance(document.querySelector(`#${this.props.id}`)).open();
        } else {
            M.Sidenav.getInstance(document.querySelector(`#${this.props.id}`)).close();
        }
    }

    componentWillUnmount() {
        M.Sidenav.getInstance(document.querySelector(`#${this.props.id}`)).destroy();
    }
    

    render() {
        return (
            <div className="sidenav" id={this.props.id}>
                {this.state.children}
            </div>
        );
    }
}

export default Sidenav;