import React from 'react';
import M from 'materialize-css';
import Aux from '../../hoc/Aux';
import Icon from './Icon';

class Modal extends React.Component {

    state = {
        children: null,
        showClose: false
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.show) {
            M.Modal.getInstance(document.querySelector(`#${this.props.id}`)).open();
        } else {
            M.Modal.getInstance(document.querySelector(`#${this.props.id}`)).close();
        }
    }

    componentDidMount() {
        const modal = this;
        const options = M.Modal.init(document.querySelector(`#${this.props.id}`)).options;
        options.onOpenStart = function () {
            modal.setState({
                children: modal.props.children
            });
        };
        options.onOpenEnd = function() {
            modal.setState({showClose: true});  
        }
        options.onCloseEnd = function () {
            modal.setState({
                children: null,
            });
            if (modal.props.onClosed) {
                modal.props.onClosed();
            }
        };
        options.onCloseStart = function() {
            modal.setState({
                showClose: false
            });
        }
        options.dismissible = this.props.dismissible !== "false"
    }

    componentWillUnmount() {
        M.Modal.getInstance(document.querySelector(`#${this.props.id}`)).destroy();
    }

    render() {
        let close = null;
        if (this.props.onClose && this.state.showClose) {
            close = (
                <div className="right-align" style={{ 
                    zIndex: 1003,
                    position: 'fixed',
                    top: '5%',
                    width: '100%',
                    right: '10%'
                }}>
                    <Icon 
                        onClick={this.props.onClose}
                        style={{ 
                        background: 'white',
                        borderRadius: '50%',
                        cursor: 'pointer'
                    }}>close</Icon>
                </div>
            );
        }

        return (
            <Aux>
                {close}
                <div className="modal" id={this.props.id}>
                    <div style={{ margin: '10px' }}>
                        {this.state.children}
                    </div>
                </div>
            </Aux>
        )
    }
}

export default Modal;