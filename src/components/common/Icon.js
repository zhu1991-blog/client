import React from 'react';

const Icon = ({children, classes, style, onClick}) => (
    <i 
        className={"material-icons " + classes} 
        style={style}
        onClick={onClick}>
        {children}
    </i>
);  

export default Icon;