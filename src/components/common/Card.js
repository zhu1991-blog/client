import React from 'react';

const Card = ({ click, title, content }) => (
    <div className="card" onClick={click}>
        <div className="card-content">
            <div className="card-title">{title}</div>
            <div>{content || "..."}</div>
        </div>
    </div>
);

export default Card;