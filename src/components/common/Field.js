import React from 'react';
import _ from 'lodash';
import { FormSelect, textareaAutoResize } from 'materialize-css';

class Field extends React.Component {

    componentDidMount() {
        if (this.props.type === 'select') {
            FormSelect.init(document.querySelector(`[name=${this.props.input.name}]`));
        } else if (this.props.type === 'textarea') {
            textareaAutoResize(document.querySelector(`[name=${this.props.input.name}]`));
        }
        if (this.props.focus) {
            document.querySelector(`[name=${this.props.input.name}]`).focus();
        }
    }

    render() {
        let field = null;
        const { props } = this;
        switch (props.type) {
            case 'select':
                field = (
                    <select id={props.input.name} {...props.input}>
                        <option value="" disabled>Select Type</option>
                        {_.map(props.options, ({ value, label }) => (
                            <option key={value} value={value}>{label}</option>
                        ))}
                    </select>
                );
                break;
            case 'textarea':
                field = <textarea id={props.input.name}  {...props.input} className="materialize-textarea" />
                break;
            default:
                field = <input id={props.input.name} type={props.type || 'text'} {...props.input} className="validate" />;
        }

        let error = null;
        if (props.meta && props.meta.error && props.meta.touched) {
            error = <span className="center-align red-text">{props.meta.error}</span>;
        }

        let classes = [];
        if (!_.isEmpty(this.props.input.value) || (props.meta && props.meta.active)) {
            classes.push('active');
        }
        return (
            <div className={'input-field ' + (this.props.classes || 'col s12')}>
                {field}
                <label className={classes.join(' ')} htmlFor={props.input.name}>{props.label}</label>
                {error}
            </div>
        );
    }
};

export default Field;