import React from 'react';
import Aux from '../../hoc/Aux';

const Section = ({title, children}) => {
    return (
        <Aux>
            <div className="divider"></div>
            <div className="section">
                <h5>{title}</h5>
                <div>{children}</div>
            </div>
        </Aux>
    );
};

export default Section;