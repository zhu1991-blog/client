import React from 'react';

const FloatingButton = (props) => {
    return (
        <div className="fixed-action-btn">
            <a  
                data-target={props.dataTarget}
                className={'btn-floating btn-large red ' + props.classes}
                onClick={props.clicked}
            >
                <i className="large material-icons">{props.icon}</i>
            </a>
        </div>
    );
};

export default FloatingButton;