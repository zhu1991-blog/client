import React from 'react';

const Spinner = ({style}) => (
    <div className="progress" style={style}>
        <div className="indeterminate"></div>
    </div>
);

export default Spinner;