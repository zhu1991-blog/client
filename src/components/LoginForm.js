import React from 'react';
import { reduxForm, Field } from 'redux-form';
import _ from 'lodash';
import { connect } from 'react-redux';

import CField from './common/Field';
import Aux from '../hoc/Aux';
import SpinnerCircular from './common/SpinnerCircular';
import * as actions from '../actions';

const FIELDS = [
    { name: "username", label: "Username"},
    { name: "password", label: "Password", type: "password"}
]

class LoginForm extends React.Component {

    state = {
        isInit: true
    }

    render() {
        const { handleSubmit, message, login, loading } = this.props;

        let spinner = null;
        if (loading) {
            spinner = (
                <span className="left">
                    <div style={{ position: 'absolute' }}>
                        <SpinnerCircular type="small" color="red" />
                    </div>
                </span>
            );
        }

        let fields = _.map(FIELDS, (field) => (
            <Field key={field.name} component={CField} {...field} />
        ));

        let error = null;
        if (message && !this.state.isInit) {
            error = <p className="center-align red-text">{message}</p>;
        }
        return (
            <Aux>
                <h4>Login</h4>
                <form onSubmit={handleSubmit(({ username, password }) => {
                    this.setState({ isInit: false });
                    login(username, password);
                })} >
                    {fields}
                    {error}
                    <div className="row">
                        <button className="col s12 m4 btn right" disabled={this.state.submitting}>
                            {spinner}
                            Login
                        </button>
                    </div>
                </form>
            </Aux>
        );
    }
};


const validate = (values) => {
    const errors = {}
    _.forEach(FIELDS, ({ name, label }) => {
        if (!values[name]) {
            errors[name] = `${label} can't empty.`
        }
    });
    return errors;
}

const config = {
    form: 'login',
    validate
}

const mapStateToProps = state => ({
    message: state.auth.error,
    loading: state.auth.loading
});

export default connect(mapStateToProps, actions)(reduxForm(config)(LoginForm))