import React from 'react';
import _ from 'lodash';
import renderHTML from 'react-render-html';

import * as dateUtil from '../../util/date';
import Chip from '../common/Chip';
import Icon from '../common/Icon'

const ProjectDetail = ({ project, onClosed }) => {
    if (!project) {
        return <div></div>;
    }

    let link = null;
    if (project.link) {
        link = <a href={project.link} target="_blank" style={{marginLeft: '20px'}}>
            <Icon style={{fontSize: '36px'}}>link</Icon>
        </a>;
    }

    let langs = _.split(project.langs, ',').map((lang, index) => (
        <Chip key={index}>{_.trim(lang)}</Chip>
    ));

    let tools = _.split(project.tools, ',').map((tool, index) => (
        <Chip key={index}>{_.trim(tool)}</Chip>
    ));

    let os = _.split(project.os, ',').map((o, index) => (
        <Chip key={index}>{_.trim(o)}</Chip>
    ));

    return (
        <div>
            <ul className="collection with-header">
                <li className="collection-header">
                    <h4>
                        {project.name}
                        {link}
                    </h4>
                </li>
                <li className="collection-item">
                    <strong>專案時程<span className="hide-on-small-only">：</span></strong>
                    <span className="hide-on-small-only"> {`${dateUtil.toString(new Date(project.startTime))} ~ ${dateUtil.toString(new Date(project.endTime))}`}</span>
                    <div className="hide-on-med-and-up"> {`${dateUtil.toString(new Date(project.startTime))} ~ ${dateUtil.toString(new Date(project.endTime))}`}</div>
                </li>
                <li className="collection-item">
                    <strong>使用技術<span className="hide-on-small-only">：</span></strong>
                    <span className="hide-on-small-only"> {langs}</span>
                    <div className="hide-on-med-and-up"> {langs}</div>
                </li>
                <li className="collection-item">
                    <strong>使用工具<span className="hide-on-small-only">：</span></strong>
                    <span className="hide-on-small-only"> {tools}</span>
                    <div className="hide-on-med-and-up"> {tools}</div>
                </li>
                <li className="collection-item">
                    <strong>專案環境<span className="hide-on-small-only">：</span></strong>
                    <span className="hide-on-small-only"> {os}</span>
                    <div className="hide-on-med-and-up"> {os}</div>
                </li>
                <li className="collection-item">
                    <p><strong>內容說明<span className="hide-on-small-only">：</span></strong></p>
                    <div
                        className={project.desc ? "": "valign-wrapper"}
                        style={{ boxSizing: 'content-box', border: '1px solid #CCC', minHeight: '200px', width: '100%', padding: '5px' }}>
                        <div className={project.desc ? "": "center-align"} style={{width: "100%", whiteSpace: 'pre-wrap'}}>
                            {project.desc ? renderHTML(project.desc) : <font color="#CCC">(待補充)</font>}
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    );
};

export default ProjectDetail;