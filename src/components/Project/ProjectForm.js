import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import _ from 'lodash';

import CField from '../common/Field';
import Icon from '../common/Icon';
import Spinner from '../common/SpinnerCircular';

const FIELDS = [
    { name: "name", label: "Name" },
    { name: "startTime", label: "Start Date", type: "date", classes: "col s12 m6" },
    { name: "endTime", label: "End Date", type: "date", classes: "col s12 m6"},
    { name: "mainLang", label: "Main Lang" },
    { name: "langs", label: "Languages" },
    { name: "tools", label: "Tools" },
    { name: "os", label: "OS" },
    {
        name: "type", 
        label: "Type", 
        type: "select", 
        options: [
            { label: "練習", value: "prac" },
            { label: "專案", value: "proj" }
        ]
    },
    { name: 'link', label: 'Link' },
    { name: "desc", label: "Desc", type: "textarea" },
];

const ProjectForm = ({ handleSubmit, onSubmit, initialValues, isAdd, submitting }) => {
    const fields = _.map(FIELDS, field => (
        <Field key={field.name} component={CField} {...field} />
    ));

    let spinner = null;
    console.log(submitting);
    if (submitting) {
        spinner = <Spinner />;
    }

    return (
        <div>
            <div className="modal-content">
                <h4>{!initialValues._id ? 'Add' : 'Edit'} Project</h4>
            </div>
            <div className="modal-footer">
                <form onSubmit={handleSubmit(onSubmit)}>
                    {fields}
                    <div className="row">
                        <div className="col s12">
                            {spinner}
                        </div>
                        <button className="col s12 m12 l3 btn right" disabled={submitting}>
                            <Icon classes="left" >save</Icon>
                            {!initialValues._id ? 'Add' : 'Save'}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}

const formConfig = {
    form: 'project'
}

const mapStateToProps = ({ project: { project, submitting } }) => ({
    initialValues: project,
    submitting
});


export default connect(mapStateToProps)(reduxForm(formConfig)(ProjectForm));