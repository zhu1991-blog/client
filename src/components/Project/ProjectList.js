import React from 'react';
import ProjectCard from './ProjectCard';
import _ from 'lodash';

const ProjectList = ({projects, onDelete, goDetail}) => (
    <div>
        {_.map(projects, project => 
            <ProjectCard 
                key={project._id} 
                {...project}
                deleted={onDelete}
                goDetail={() => goDetail(project)}
                truncate
            />
        )}
    </div>
)

export default ProjectList;