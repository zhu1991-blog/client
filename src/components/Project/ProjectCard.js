import React from 'react';

import * as dateUtil from '../../util/date';
import Icon from '../common/Icon';
import Card from '../common/Card';

const ProjectCard = (project) => {
    const descClass = [];
    let nameClass = null;
    const nameStyle = {};
    let contentClass = ['valign-wrapper'];
    let deleteBtn = null;
    if (project.deleted) {
        deleteBtn = (
            <div className="right-align col s12 m5">
                <button
                    className="btn-flat red-text"
                    onClick={(e) => {
                        e.stopPropagation();
                        if (window.confirm(`確定刪除 ${project.name}?`)) {
                            project.deleted(project._id);
                        }
                    }}
                >
                    <Icon classes="left">delete_forever</Icon>
                    Delete
                </button>
            </div>
        );
        descClass.push('col s12 m7');
        contentClass.push('row');
    }

    if (project.truncate) {
        descClass.push('truncate');
        nameClass = 'truncate';
    }

    let time = null;
    let mainLang = null;
    if (project.startTime && project.endTime) {
        time = (
            <span className="right hide-on-med-and-down" style={{fontSize:'60%'}}>
                {`${dateUtil.toString(new Date(project.startTime))} ~ ${dateUtil.toString(new Date(project.endTime))}`}
            </span>
        );
        nameStyle['display'] = 'inline';
    } else if (project.mainLang) {
        mainLang = (
            <div
                className="grey lighten-3"
                style={{
                    fontSize: '14px', 
                    display: 'inline-block',
                    borderRadius: '5px',
                    padding: '0 5px'
                }}
            >
                <Icon 
                    classes="left light-green-text darken-1" 
                    style={{
                        marginRight: '5px', 
                        fontSize: '10px'
                    }}
                >
                        brightness_1
                </Icon>
                {project.mainLang}
            </div>
        );
    }

    return (
        <Card
            click={project.goDetail}
            title={
                <span>
                    <span 
                        className={nameClass}
                        style={nameStyle}
                    >
                        {project.name}
                    </span>
                    {time || mainLang}
                </span>
            }
            content={
                <div className={contentClass.join(' ')} style={{ marginBottom: 0 }}>
                    <div className={descClass.join(' ')}>
                        {project.desc ? project.desc : "..."}
                    </div>
                    {deleteBtn}
                </div>
            }
        />
    );
};

export default ProjectCard;