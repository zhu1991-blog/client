export const toString = (date, separate = "/") => (
    `${date.getFullYear()}${separate}${((date.getMonth() > 9) ? "" : "0") + (date.getMonth() + 1)}${separate}${((date.getDate() >= 10) ? "": "0") + date.getDate()}`
);